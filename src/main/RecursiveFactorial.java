package main;

public class RecursiveFactorial {

	public RecursiveFactorial() {
	}

	public long factorialize(int num) {
		long result = 0;
		if (num > 1) {
			result = num * factorialize(num - 1);
			//System.out.println(result);
			return result;
		} else {
			return 1L;
		}
	}
	
}

/*

num = 4

first return: num = 1
return = 1
result = 1

second return: num = 2
return = 2*1 (2)
result = 2*1 (2)

third return:
num = 3
return = 3*2 (6)
result = 3*2 (6)

fourth return:
num = 4
return = 4*3 (12)
result = 4*6 (24)

*/