package main;

public class LoopFactorial {

	public LoopFactorial() {
	}
	
	public long factorialize(int num) {
		long result = 1;
		for (int i = 1; i <= num; i++) {
			if (num == 1) break;
			result *= i;				// this is why 'i' and 'result' have to be 1 initially
			//System.out.println(result);
		}
		return result;
	}
	
}

/*

num = 5

first loop:
num = 5
result = 1*5 (5)
num = 4

second loop:
num = 4
result = 5*4 (20)
num = 3

third loop:
num = 3
result = 20*3 (60)

fourth loop
num = 2
result = 60*2 (120)

*/