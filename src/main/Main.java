package main;

public class Main {

	public static void main(String[] args) {

		int runs = 40000;
		long totalDifference = 0;
		long runStart;
		long runEnd;
		
		runStart = System.nanoTime();
		for (int i = 0; i < runs; i++) {
			totalDifference += calculate();
		}
		runEnd = System.nanoTime();
		System.out.println("Recursion was faster by " + totalDifference + "ns over "+ runs + " runs");
		System.out.println("Total run time: " + (runEnd - runStart) + "ns");
	}
	
	public static long calculate() {
		int num = 20;
		long loopStart;
		long loopEnd;
		long loopTime;
		
		long recursionStart;
		long recursionEnd;
		long recursionTime;
		
		long difference;
		
		// loop
		System.out.println("Factorial of " + num + " using Foor Loop: ");
		LoopFactorial loop = new LoopFactorial();
		
		loopStart = System.nanoTime();
		System.out.println("Result: " + loop.factorialize(num));
		loopEnd = System.nanoTime();
		loopTime = loopEnd - loopStart;
		
		System.out.println("Time taken: " + loopTime + "ns");

		// recursive
		System.out.println("\nFactorial of " + num + " using Recursion: ");
		RecursiveFactorial recursive = new RecursiveFactorial();
		
		recursionStart = System.nanoTime();
		System.out.println("Result: " + recursive.factorialize(num));
		recursionEnd = System.nanoTime();
		recursionTime = recursionEnd - recursionStart;
				
		System.out.println("Time taken: " + recursionTime + "ns");
		
		difference = loopTime < recursionTime ? (recursionTime - loopTime) : (loopTime - recursionTime);
	
		System.out.println(
		loopTime < recursionTime ? "\n - Loop was faster by " + difference + "ns\n"
								 : "\n - Recursion was faster by " + difference + "ns\n"
		);
		
		if (loopTime > recursionTime) {
			return difference;
		} else {
			return -difference;
		}
	}

}
