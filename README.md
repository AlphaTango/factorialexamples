# Factorial Examples #

An example of implementating factiorial calculation using both a For Loop and Recursion; this is another common interview question.

### How do I run this? ###

Download the source, compile and run main.java

### What is this repository for? ###

This is a quick demonstration of how you can calculate factorials in Java. The main class has a field called 'runs' which you can adjust to run the example multiple times. On small runs (<10), recursion seems to have the edge, however surprisingly on larger runs loops seem to perform better - probably due to compiler optimization (e.g. loop unrolling) after interpreting the bytecode. It may also be due to better cache usage, but the difference is so slight (generally 0 - 20,000 nanoseconds) that it could equally be due to environmental factors (such CPU usage by the OS in the background).